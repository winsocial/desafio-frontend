# Processo Seletivo WinSocial - Frontend Developer

## Sobre o Teste

A plataforma fictícia **WinRent** quer ser um novo player forte no mercado imobiliário digital. No roadmap desse ano está abocanhar o mercado de compra, venda e aluguel de imóveis pelo mundo.

Seu papel aqui é ajudar a Product Owner a subir rapidamente um MVP para apresentar o modelo aos stakeholders.

## Instruções
Dividimos o teste em etapas. Sinta-se à vontade para ir até onde você desejar! 😉
#### Etapa 1
A etapa 1 consiste em traduzir estáticamente a primeira e a última sessão do [protótipo do figma](https://www.figma.com/file/qwJONSqC5RXCHSQefWaCes/DevTest) utilizando o framework ReactJs. São elas: 

- Header
- Compre, alugue, ou venda o seu imóvel
- Sitemap
- Footer

#### Etapa 2
Na etapa 2, incluiremos as listagens de propriedades e depoimentos. Elas são alimentadas dinamicamente através de serviços. São elas:

- Lista de cidades na seção **Com base na sua localização**: nesse caso, basta listar as 6 primeiras (ids de 1 a 6) do serviço [Addresses Api](apis/addresses.json).
- Lista de Depoimentos: os dados necessários são encontrados na [Testimonies Api](apis/testimonies.json)

Você já deve ter percebido que não estamos disponibilizando APIs para esse teste. Os arquivos da pasta `apis` são nossas apis fictícias. Seria legal ver no código uma estrutura de `Services` que pudesse ser facilmente substituída por APIs reais.
#### Etapa 3 (plus)
A única feature funcional da landing page será o buscador de aluguéis. Nele, o usuário poderá iniciar a digitação de uma cidade e ver uma lista com os resultados no formato `Cidade, País`. Essas cidades são obtidas através da api [Addresses Api](apis/addresses.json). Os dados necessários são: `city` e `country`.
#### Etapa 4 (plus)
Um plus seria que o selecionador de datas do buscador desabilitasse as opções de datas que já estejam alugadas. Esses dados você encontra no [Booking Api](apis/bookings.json)
