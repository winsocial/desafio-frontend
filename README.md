# Desafio para Frontend - winsocial.com

Este documento descreve o exercício de programação para a vaga de Frontend Developer da winsocial.com.

## Considerações sobre o desafio

* O prazo para entrega da solução é de 3 dias corridos, contados a partir da data de recebimento do desafio. Caso você precise de mais tempo, entre em contato conosco e fechamos uma nova data para a entrega. Não se preocupe, somos super flexíveis ;)
* Utilize um dos seguintes frameworks: React
* O código produzido deve estar versionado em algum repositório público (Github, Bitbucket etc.)
* Quando estiver tudo pronto, você deve mandar um e-mail para joaogc@winsocial.com.br com o assunto `Frontend Developer - <%SEU_NOME%>` e o link para o seu repositório.

## Desafio

O desafio pode ser acessado pelo [link](desafio-tecnico.md).

## Documentação e Deploy

Esperamos também um passo a passo de como executar a sua solução. Quanto mais simples, melhor. Vale ressaltar que a execução **não poderá depender do uso de alguma IDE específica**.

## Avaliação

A sua solução será revisada numa call contigo pelo time de desenvolvedores e designers aqui da **WinSocial**.

Esperamos que você se divirta codificando essa solução. Estou aqui (joaogc@winsocial.com.br) caso surjam dúvidas durante o desenvolvimento.

Bom código! ;)
